Name:     douda_config
Version:  0.1
Release:  1
Summary:  Some personal configuration files
License:  GPLv3+
URL:      http://nope.nope
Source0:  dotfiles.tar.bz2
Requires: accountsservice, xorg-x11-server-Xorg, sudo

%prep
%setup -q -n dotfiles

%install
rm   -rf    $RPM_BUILD_ROOT
mkdir -p    $RPM_BUILD_ROOT/home/
cp -r chedi $RPM_BUILD_ROOT/home/

%posttrans
cat >> /var/lib/AccountsService/users/chedi << EOF
[User]
Language=en_US.utf8
XSession=xmonad
Icon=/var/lib/AccountsService/icons/chedi
SystemAccount=false
EOF

cat >> /etc/X11/xorg.conf.d/00-keyboard.conf << EOF
Section "InputClass"
        Identifier "system-keyboard"
        MatchIsKeyboard "on"
        Option "XkbLayout" "fr"
EndSection
EOF

cp /home/chedi/Pictures/Avatar/chedi /var/lib/AccountsService/icons/

sed -i -e "s/^\(%wheel.*\)ALL$/\1 NOPASSWD: ALL/" /etc/sudoers

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,1000,1000)
/home/chedi

%description
Various configuration files

%changelog
* Thu Feb 12 2015 Douda <chedi.toueiti@gmail.com> - 0.1
- Initial version of the package
