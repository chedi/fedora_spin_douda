Personal Fedora SPIN:
=====================

This project covers the creation of a custom fedora SPIN inculuding some of my most used tools
and with an automatic creation of the main user home folder and configuration files.


Some prequisites are necessairy to get this working:
 * Fedora packages on the build machine
  * grub2
  * rpm-build
  * createrepo
  * livecd-tools
  * fedora-packager
  * qemu (optional to the build but to test the result)


First step is to identify what are the packages you want to include in the liveimage and how big
you can afford it to be. For me, I only need the xmonad wm and emacs plus some python minimal
installation.

The file douda.ks is the kickstart file that define the list of packages to include and remove under
the %packages section. You will notice that I included multiple third partie repositories just before
that, this means that you will have access to any of the packages in those repositories and you can
others too.

Just note that the first repo is a local one and in which you will have a custom made rpm with your
personal files (see the section about personal files bellow).

customizign the douda.ks file enables you to configure other important information about the livecd
image like the hostname or default user password and adding it to the wheel group (for sudo permission).

PS: if you change the user name don't forget to change all the paths relative to the home folder
/home/chedi


Packaging your custom files:
----------------------------

To add the files you need just create a compressed file containing a folder named after you desired user.
So for example:
 * Create the first folder named dotfiles
 * Create a second folder named chedi (my user name) under dotfiles
 * Copy all your home file structure under the chadi folder
 * Compress the dotfiles folder using tar xjf dotfiles.tar.bz2 dotfiles/

The reason to to all this is because most of the configuration file are hidden  (begining with a dot)
so if you do a simple copy * in the rpm creation process they simply wouldn't be transfered, And going
to the length of copying each one of them by specific name is not effecient and error prone.

To start building the rpm you have to:
 * initialize the enviornment via rpmdev-setuptree
 * copy the source file dotfiles.tar.bz2 under rpmbuild/SOURCES
 * start the build with the command:
```
rpmbuild -ba rpmbuild/SPECS/douda_config.spec
```

My spec file does some extra configuration step that you have to be aware of:
 * Change the default X keyboard to french
 * Changes the avatare of the user with a custom one

For that reason the package is dependent on accountservice and xorg-x11-server-Xorg.

Creating your custom repository:
--------------------------------

To create your own rpm repository just copy the rpms you need from the rpmbuild/RPMS/ folder
to somewhere else and execute the command:
createrepo .


Starting the livecd build process:
----------------------------------

Once everthing is set you can start building your own livecd using the command
```
sudo livecd-creator douda.ks -f douda --verbose --cache=/var/tmp
```
You have to notice that the procedure requires a netwotk connection to download
the rpm packages. Adding the --cache argument will prevent the redownloading of
already fresh packages. The -f option is for the disk label and can be changed at
wish.

If you encounter a problem of space on the build process just adjust the build image
size at the 9th line of the config file.

The result of the process is an iso file that you can test with qemu using the command
```
qemu-kvm -m 2048 -vga qxl -cdrom douda.iso
```

Extra:
------

If you like me have a thumb drive with all the goods you can add this entry to your grub.cfg
```
menuentry 'Fedora 21 douda' {
	insmod loopback
	insmod iso9660
	set isofile="/boot/douda.iso"
	loopback loop $isofile
	linux (loop)/isolinux/vmlinuz0 root=live:CDLABEL=douda rootfstype=auto ro rd.live.image rd.luks=0 rd.dm=0 iso-scan/filename=${isofile}
	initrd (loop)/isolinux/initrd0.img
}
```

if you decide to transfert you image to a hard drive just type the command liveinst
